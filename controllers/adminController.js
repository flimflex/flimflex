const User=require('../models/userModels');
const bcrypt=require('bcrypt');
const randomstring=require('randomstring')
const config=require('../config/config')
const nodemailer=require('nodemailer') 

//making admin login page method 
const loadLogin=async(req,res)=>{
    try {
        
        res.render('login')

    } catch (error) {
        console.log(error.message)
    }
}

//making aming login verification
const verifyLogin=async(req,res)=>{
    try {
        
        const email=req.body.email;
        const password=req.body.password;

        const userData=await User.findOne({email:email})
        if(userData){

            const passwordMatch=await bcrypt.compare(password,userData.password);
            if(passwordMatch){
                if(userData.is_admin===0){
                    res.render('login',{message:'Email and Password is incorrect'})
                }
                else{
                    req.session.user_id=userData._id;
                    res.redirect('/admin/home')
                }
            }
            else{
                res.render('login',{message:"Email and Password is incorrect."})       
            }

        }
        else{
            res.render('login',{message:"Email and Password is incorrect."})
        }

    } catch (error) {
        console.log(error.message)
    }
}

//methode to load home page
const loadHome=async(req,res)=>{
    try {
        
        res.render('home')

    } catch (error) {
        console.log(error.message)
    }
}

//amdin logout method
const logout=async(req,res)=>{
    try {
        req.session.destroy();
        res.redirect('/admin')

    } catch (error) {
        console.log(errro.message)
    }
}

//forgot load methode
const forgotLoad=async(req,res)=>{
    try {
        res.render('forgot')

    } catch (error) {
        console.log(error.message)
    }
}

//for reset password send mail
const sendResetPasswordMail=async(name,email,token)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailpassword
            }
        })
        const mailOptions={
            from:config.emailUser,
            to:email,
            subject:"For Reset Password link",
            
            html:'<p>Hi '+ name + 'please chick her to <a href="http://localhost:5000/admin/forgot-password?token='+token+'"> Reset</a>your passwrod</P> '
        }
        transporter.sendMail(mailOptions,function(error,info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:-",info.response)
            }
        })
    } catch (error) {
        console.log(error.message)
    }
}  


//verifying forgot method 
const verifyForgot=async(req,res)=>{
    try {
        
        const email=req.body.email;
        const userData=await User.findOne({email:email})

        if(userData){
            if(userData.is_admin===0){
                res.render('forgot',{message:"Email is incorrect"})
            }
            else{
                const randomString=randomstring.generate();
                const updatedData=await User.updateOne({email:email},{$set:{token:randomString}});
                sendResetPasswordMail(userData.name,userData.email,randomString);
                // res.render('forgot',{message:"please check your mail to reset your password"})
                res.redirect('AdminForgotConfirmation')
            }
        }
        else{
            res.render('forgot',{message:"Email is incorrect."})
        }
    } catch (error) {
        console.log(error.message)
    }
}

// methode to load forgot password ejs
const loadForgotPassword=async(req,res)=>{
    try {
        const token=req.query.token;
        const tokenData= await User.findOne({token:token})
        if(tokenData){
            res.render('forgot-password',{user_id:tokenData.id})
        }
        else{
            res.render('404',{message:"Invalid Link"})
        }
       
    } catch (error) {
        console.log(error.message)
    }
}


//making funciton to hash the password
const hashPasswod=async(password)=>{
    try {
        const passwordHash=await bcrypt.hash(password,10);
        return passwordHash;

    } catch (error) {
        console.log(error.message)
    }
}

//-------------------------------------------------------//
//method to resetPassword view
const resetPassword=async(req,res)=>{
    try {
        
        const password=req.body.password;
        const user_id=req.body.user_id;

        const securePassword= await hashPasswod(password);

        const updatedData=  await User.findByIdAndUpdate({_id:user_id},{$set:{password:securePassword,token:"" }})
        res.render('AdminResetConfrimation')

    } catch (error) {
        console.log(error.message)
    }
}

//making method to load add movie page
const loadAddMovie=async(req,res)=>{
    try {
        
        res.render('addMovie')

    } catch (error) {
        console.log(error.message)
    }
}
//method to add movie to the mongodb
const Movie=require('../models/addMovie')
const insertMovie=async(req,res)=>{
    try {
        const movie=Movie({
            title:req.body.title,
            releasedDate:req.body.releasedDate,
            genre:req.body.genre,
            price:req.body.price,
            show_start_date:req.body.show_start_date,
            show_end_date:req.body.show_end_date,
            show_start_time:req.body.show_start_time,
            show_end_time:req.body.show_end_time,
            image:req.file.filename,
            description:req.body.description,
        })
    
        const movieData= await movie.save();
        console.log(movieData)
        if(movieData){
            res.render('addMovie',{message:"successfully added movie"})
        }
        else{
            res.render('addMovie',{message:"Failed to add movie"})
        }
    } catch (error) {
        console.log(error.message)
    }
}
//method to load movie management page
const loadMovieManagement = async(req,res)=>{
    try {
        const movies = await Movie.find();
        res.render('movie-Management', {
            title: 'FlimFlex',
            movies: movies,
        });
    } catch (error) {
        console.log(error.message)
    }
}
//method to load booking management page
const loadBookingManagement=async(req,res)=>{
    try {
        res.render('booking-management')
    } catch (error) {
        console.log(error.message)
    }
}
//-------------------------------------------------//
const loadAdminForgotConfirmation = async(req,res) =>{
    try {
        //loading register page
        res.render('AdminForgotConfirmation')
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
const loadAdminResetConfirmation = async(req,res) =>{
    try {
        //loading register page
        res.render('ResetConfirmationPage')
    } catch (error) {
        console.log(error.message)
    }
}
//----------------------------------------------//
const loadUserManagement = async (req, res) => {
    try {
        const users = await User.find({ is_admin: 0 });
        res.render('userManagement', {
            title: 'FlimFlex',
            users: users,
        });
    } catch (error) {
        console.log(error.message)
    }
};


//----------------------------------------------------------------//

const multer=require('multer');
const fs = require('fs');

const deleteUser=async(req,res)=>{
    const id = req.params.id;
    try {
        const user = await User.findById(id);

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        const result = await User.findByIdAndRemove(id);

        if (user.image !== '') {
            const imagePath = "./public/userImages/" + user.image;
            try {
                await fs.promises.unlink(imagePath); // Using fs.promises for async file operations
            } catch (err) {
                console.error('Error deleting image:', err);
            }
        }
        res.redirect('/admin/userManagement');
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
}
const deleteMovie=async(req,res)=>{
    const id = req.params.id;
    try {
        const movie = await Movie.findById(id);

        if (!movie) {
            return res.status(404).json({ message: 'Movie not found' });
        }

        const result = await Movie.findByIdAndRemove(id);

        if (movie.image !== '') {
            const imagePath = "./public/MovieImages/" + movie.image;
            try {
                await fs.promises.unlink(imagePath); // Using fs.promises for async file operations
            } catch (err) {
                console.error('Error deleting image:', err);
            }
        }
        res.redirect('/admin/movieManagement');
    } catch (err) {
        console.error(err);
        res.status(500).json({ message: 'Internal server error' });
    }
}



//----------------------------------------------//
module.exports={
    loadLogin,verifyLogin,loadHome,logout,forgotLoad,verifyForgot,loadForgotPassword,resetPassword,loadAddMovie,
    insertMovie,loadMovieManagement,loadBookingManagement,loadAdminForgotConfirmation,loadAdminResetConfirmation,
    loadUserManagement,deleteUser,deleteMovie
}