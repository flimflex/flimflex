//import userModel form models
const User= require("../models/userModels") 
const bcrypt=require('bcrypt');
const nodemailer=require('nodemailer')
const randromstring=require('randomstring')
const config=require("../config/config")


//----------------------------------------------------------------//
//making email sending function  
const sendVerifyMail=async(name,email,user_id)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailpassword
            }
        })
        const mailOptions={
            from:config.emailUser,
            to:email,
            subject:"For verification mail",
            html:'<p>Hi '+name+ 'please chick her to <a href="http://localhost:5000/verify?id='+user_id+'">Verify</a>your mail.</P> '
        }
        transporter.sendMail(mailOptions,function(error,info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:-",info.response)
            }
        })
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//for reset password send mail
const sendResetPasswordMail=async(name,email,token)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailpassword
            }
        })
        const mailOptions={
            from:config.emailUser,
            to:email,
            subject:"For Reset Password link",
            
            html:'<p>Hi '+ name + 'please chick her to <a href="http://localhost:5000/forgot-password?token='+token+'"> Reset</a>your passwrod</P> '
        }
        transporter.sendMail(mailOptions,function(error,info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:-",info.response)
            }
        })
    } catch (error) {
        console.log(error.message)
    }
} 

//----------------------------------------------------------------//
//making funciton to hash the password
const hashPasswod=async(password)=>{
    try {
        const passwordHash=await bcrypt.hash(password,10);
        return passwordHash;

    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//Loading resigtration page
const loadRegister = async(req,res) =>{
    try {
        //loading register page
        res.render('registration')
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//adding user
const insertUser=async(req,res)=>{
    try {
        //caling hashPassword function
        const securePassword= await hashPasswod(req.body.password);
        const user =User({
        
            // getting the form data using name attribute from the form
            name:req.body.name,
            phoneNumber:req.body.phoneNumber,
            email:req.body.email,
            password:securePassword ,
            image:req.file.filename,
            is_admin:0,
        })
        const userData= await user.save();

        if(userData){
            sendVerifyMail(req.body.name,req.body.email,userData._id);
            // res.render('registration',{message:"Your registration has been successfully.Please verify you email."});
            res.redirect('/signupConfirmation')
            console.log("registration successful")
        }
        else{
            res.render('registration',{message:"Your registration has been failed. "})
        }
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//making verify funciton 
const verifyMail=async(req,res)=>{
    try {

        const updateInfo= await User.updateOne({_id:req.query.id},{$set:{ is_verified:1 }});
        console.log(updateInfo)
        res.render("email-verified")
        
    } catch (error) {
        console.log(error)
    }
}

//----------------------------------------------------------------//
//login user methods started
const loginLoad=async(req,res)=>{
    try {
        
        res.render('login')

    } catch (error) {
        console.log(erro.message)
    }
}
//----------------------------------------------------------------//
const verifyLogin=async(req,res)=>{
    try {
        const email=req.body.email;
        const password=req.body.password;

        const userData= await User.findOne({ email:email });
        if (userData) {
            
            const passwordMatch=await bcrypt.compare(password,userData.password);
            if(passwordMatch){
                if (userData.is_verified===0) {
                    res.render('login',{message:"please verify your mail"});
                } else {
                    req.session.user_id=userData._id;
                    res.redirect('/home');
                }
            }
            else{
                res.render('login',{message:"Email and password is incorrect."});
            }

        } else {
            res.render('login',{message:"Email and password is incorrect."});
        }

    } catch (error) {
        console.log(error.message)
    }
}
//----------------------------------------------------------------//
//loading home page
const loadHome=async(req,res)=>{
   
    try {
       const userData= await User.findById({_id:req.session.user_id})
       const movies = await Movie.find();
        res.render('home',{user:userData,
            title: 'FlimFlex',
            movies: movies,
        })

    } catch (error) {
        console.log(error.message)
    }
}

//-----------------------------------------------------------------//
const userLogout=async(req,res)=>{
    try {
        req.session.destroy();
        res.redirect('/')
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//forgot password code start
const forgotLoad=async(req,res)=>{
    try {

        res.render('forgot')
        
    } catch (error) {
        console.log(error.message)
    }
}
//----------------------------------------------------------------//
//forgot verify method
const forgotVerify=async(req,res)=>{
    try {
        //getting email form forgot view
        const email=req.body.email;

        //finding the email in the database
        const userDate = await User.findOne({email:email});
        if(userDate){

            //checking email is verified or not
            if(userDate.is_verified===0){
                res.render('forgot',{message:"Please verify your email."})
            }
            else{
                //generating random string token
                const randomString= randromstring.generate();
                //updating the token field using email as a parameter
                const updatedData=await User.updateOne({email:email},{$set:{ token:randomString }});

                //calling a sendResetPasswordMail 
                sendResetPasswordMail(userDate.name,userDate.email,randomString);
                // res.render('forgot',{message:"Please check your mail to reset password"})
                res.redirect('ForgotConfirmationPage')
                console.log("mail send successfully");

            }
        }
        else{
            //printing error message
            res.render('forgot',{message:"User email is incorrect."})
        }

    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//forgotPasswordLoad method
const forgotPasswordLoad=async(req,res)=>{
    try {
        //retrieveing toeken from url
       const token =req.query.token;
        const tokenData= await User.findOne({token:token})
        if(tokenData){
            res.render('forgot-password',{user_id:tokenData.id})
            console.log(tokenData.id)
        }
        else{
            res.render("404",{message:"Invalid token"})
        }
    } catch (error) {
        console.log(error.message)
    }
}
//----------------------------------------------------------------//
//reset password methode
const resetPassword=async(req,res)=>{
    try {
        
        const password=req.body.password;
        const user_id=req.body.user_id;
        const  secure_password=await hashPasswod(password);
        
        const updatedData=await User.findByIdAndUpdate({_id:user_id},{$set:{password:secure_password,token:""}});
        res.redirect('/resetConfirmationPage')

    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//load SignupConfirmation
const loadSignupConfirmation = async(req,res) =>{
    try {
        //loading register page
        res.render('SignupConfirmation')
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//

const loadForgotConfirmation = async(req,res) =>{
    try {
        //loading register page
        res.render('ForgotConfirmationPage')
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
const loadResetConfirmation = async(req,res) =>{
    try {
        //loading register page
        res.render('ResetConfirmationPage')
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
const loadProfile = async (req, res) => {
    try {
        const userData = await User.findById({ _id: req.session.user_id });
        // Check if userData is defined before rendering the profile page
        if (userData) {
            res.render('profile', { user: userData });
        } else {
            // Handle the case where userData is not found (e.g., user not found)
            res.render('error', { message: "User data not found." });
        }
    } catch (error) {
        console.log(error.message);
        // Handle the error appropriately, e.g., render an error page
        res.render('error', { message: "An error occurred while loading the profile." });
    }
};

//----------------------------------------------------------------//
//loading movie page
const Movie=require('../models/addMovie')
const loadMoviePage = async(req,res)=>{
    try {
        const movies = await Movie.find();
        res.render('movie', {
            title: 'FlimFlex',
            movies: movies,
        });
    } catch (error) {
        console.log(error.message)
    }
}
//----------------------------------------------------------------//
const loadMovieDetais=async(req,res)=>{
    try {
        const movies = await Movie.find();
        const id = req.params.id;
        const movie = await Movie.findById(id);

        if (!movie) {
            return res.status(404).json({ message: 'Movie not found' });
        }
        else{
           
            res.render('movieDetails',{ movie: movie,movies:movies })
            
        }
    } catch (error) {
        console.log(error.message)
    }
}


//----------------------------------------------------------------//
const loadBookingPage=async(req,res)=>{
    try {
        const movies = await Movie.find();
        const id = req.params.id;
        const movie = await Movie.findById(id);

        if (!movie) {
            return res.status(404).json({ message: 'Movie not found' });
        }
        else{
            const startDate = new Date(movie.show_start_date); // Assuming the first feature's start date represents the movie's start date
            const endDate = new Date(movie.show_end_date); // Assuming the first feature's end date represents the movie's end date
            const dateOptions = [];

        // Generate date options within the range
        while (startDate <= endDate) {
            dateOptions.push(startDate.toISOString().split('T')[0]);
            startDate.setDate(startDate.getDate() + 1);
        }
            res.render('Booking',{ movie: movie,movies:movies,dateOptions })
        }
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
const loadTheaterPage=async(req,res)=>{
    try {
        res.render("Theater")
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
const loadContactUspage=async(req,res)=>{
    try {
        res.render("ContactUs")
    } catch (error) {
        console.log(error.message)
    }
}

//----------------------------------------------------------------//
//adding contact us information
const contactUs=require('../models/contactUs')

const insertContactUs=async(req,res)=>{
   try {
    const contactUsInfo =contactUs({
        
        // getting the form data using name attribute from the form
        name:req.body.name,
        subject:req.body.subject,
        message:req.body.message,
        
    })
    const infoData= await contactUsInfo.save();

    if(infoData){
        res.render('ContactUs',{message:"Message has been sent successfully!"})
    }
    else{
        res.render('ContactUs',{message:"Failed to send a message"})
    }

   } catch (error) {
    console.log(error.message)
   }
}
//----------------------------------------------------------------//
module.exports={
    loadRegister,
    insertUser,
    verifyMail,
    loginLoad,
    verifyLogin,
    loadHome,
    userLogout,
    forgotLoad,
    forgotVerify,
    forgotPasswordLoad,
    resetPassword,
    loadSignupConfirmation,
    loadForgotConfirmation,
    loadResetConfirmation,
    loadProfile,
    loadMoviePage,
    loadMovieDetais,
    loadBookingPage,
    loadTheaterPage,
    loadContactUspage,insertContactUs
}