//importing libraries
const mongoose=require("mongoose");

//using mongoose schema to create user Schema
const contactUsSchema= new mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    subject:{
        type:String,
        required:true
    },
    message:{
        type:String,
        required:true
    },

})
module.exports=mongoose.model("contactUs",contactUsSchema);
