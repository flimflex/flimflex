// import the libraries
const express=require("express");
const mongoose=require("mongoose");

const app=express();
const port=5000;

//----------------------------------------------------------//
//Database connection
mongoose.connect("mongodb://127.0.0.1:27017/movie_project",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => {
    console.log('Connected to MongoDB successfully');
})
.catch((error) => {
    console.error('Error connecting to MongoDB:', error);
});

//------------------------------------------------------//
//UserRoutes
const userRoute=require('./routes/userRoute')
app.use('/',userRoute );

//-------------------------------------------------------//
//admin routes
const adminRoute=require('./routes/adminRoute')
app.use('/admin',adminRoute );

//------------------------------------------------------//
app.listen(port,()=>{
    console.log("Server running at port" +" "+ port)
})
