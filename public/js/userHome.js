$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        dots:false,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }
    })

    var $grid = $(".grid").isotope({
        itemSelector:'.grid-item',
        layoutMode: 'fitRows' 
    })

    //filter items on button click
    $(".button-group").on("click","button", function(){
        var filterValue=$(this).attr('data-filter');
        $grid.isotope({ filter:filterValue });
    })
})  