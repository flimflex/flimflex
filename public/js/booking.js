const seatsContainer = document.getElementById("seats-container");
const statusDiv = document.getElementById("status");

const seatPrice = 300; // Price per seat
const totalSeats = 48;
const bookedSeats = new Set();

function createSeats() {
    for (let i = 1; i <= totalSeats; i++) {
        const seat = document.createElement("div");
        seat.className = "seat";
        seat.innerText = i;
        seat.addEventListener("click", () => {
            if (!bookedSeats.has(i)) {
                bookedSeats.add(i);
                seat.classList.add("booked");
            } else {
                bookedSeats.delete(i);
                seat.classList.remove("booked");
            }
            updateStatus();
        });
        seatsContainer.appendChild(seat);
    }
}

function updateStatus() {
    const bookedCount = bookedSeats.size;
    const totalPrice = bookedCount * seatPrice;
    statusDiv.innerText = `Total Price: Nu.${totalPrice}`;
}

createSeats();
updateStatus();
