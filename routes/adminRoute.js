//importing the necessicity libraries
const express=require('express');
const admin_route=express();
const adminController=require('../controllers/adminController')
const auth=require('../middleware/adminAuth')


//session code
const session =require('express-session')
const config=require('../config/config')
admin_route.use(session({secret:config.sessionSecret}))


//body parser code
const bodyParser=require('body-parser');
admin_route.use(bodyParser.json());
admin_route.use(bodyParser.urlencoded({extended:true}));

admin_route.set('view engine','ejs');
admin_route.set('views','views/admin');

//displayin the admin page
admin_route.get('/',auth.isLogout,adminController.loadLogin)

//posting login details like email and password
admin_route.post('/',adminController.verifyLogin)

//setting home routes
admin_route.get('/home',auth.isLogin,adminController.loadHome) 

//admin logout route
admin_route.get('/logout',auth.isLogin,adminController.logout)

//admin forgot password routes
admin_route.get('/forgot',auth.isLogout,adminController.forgotLoad)
admin_route.post('/forgot',adminController.verifyForgot)
//get forget-passowrd
admin_route.get('/forgot-password',adminController.loadForgotPassword)
//posting reset password form data
admin_route.post('/forgot-password',auth.isLogout,adminController.resetPassword)

//___________________________________________________________________________________//
admin_route.get('/addMovie',auth.isLogin,adminController.loadAddMovie)

//import and using multer
const multer=require('multer');
const path = require('path');


const storage=multer.diskStorage({
    destination:function(req,filename,cb){
        cb(null,path.join(__dirname,'../public/MovieImages'));
    },
    filename:function(req,file,cb){
        const name=Date.now()+'-'+file.originalname;
         cb(null,name);
    }
})
//uploading image 
const upload=multer({
    storage:storage,
})

admin_route.post('/addMovie',upload.single('image'),adminController.insertMovie)

//_________________________________________________________________________________//
//getting a movie management
admin_route.get('/movieManagement',auth.isLogin,adminController.loadMovieManagement)

//Booking managing routes
admin_route.get('/bookingManagement',auth.isLogin,adminController.loadBookingManagement)

//routes for getting forgot password confirmation page
admin_route.get('/AdminForgotConfirmation',auth.isLogout, adminController.loadAdminForgotConfirmation);

//routes for getting forgot password confirmation page
admin_route.get('/AdminResetConfirmation',auth.isLogout, adminController.loadAdminResetConfirmation);

//routes for getting user management page
admin_route.get('/userManagement',auth.isLogin,adminController.loadUserManagement)


//deleting a particular user
admin_route.get('/delete/:id',auth.isLogin,adminController.deleteUser)

//deleting a particular user
admin_route.get('/deleteMovie/:id',auth.isLogin,adminController.deleteMovie)



module.exports=admin_route;