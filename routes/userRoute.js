//importing libraries
const express =require("express");
const user_route=express();
const session = require("express-session");
const config=require('../config/config')

//defining session for the users
user_route.use(session({
    secret:config.sessionSecret,// Change this to your actual secret key
    resave: false, // Set resave option to false
    saveUninitialized: true, // Set saveUninitialized option to true
}));

//setting view engine
user_route.set('view engine','ejs')
user_route.set('views','./views/users')

//importing body parser
const bodyParser=require('body-parser')

user_route.use(bodyParser.json());
user_route.use(bodyParser.urlencoded({extended:true}))

//import and using multer
const multer=require('multer');
const path = require('path');


//--------------------------------------------------------------//

//retrieveing image
user_route.use(express.static('public'));
 

//--------------------------------------------------------------// 
const storage=multer.diskStorage({
    destination:function(req,filename,cb){
        cb(null,path.join(__dirname,'../public/userImages'));
    },
    filename:function(req,file,cb){
        const name=Date.now()+'-'+file.originalname;
         cb(null,name);
    }
})
//uploading image 
const upload=multer({
    storage:storage,
})

//middleware (session auth)
const auth=require('../middleware/auth') 

//importing userController form controllers
const userController=require("../controllers/userController");
 
user_route.get('/register',auth.isLogout, userController.loadRegister);

user_route.post('/register',upload.single('image'),userController.insertUser);

user_route.get('/verify',userController.verifyMail);

user_route.get('/',auth.isLogout,userController.loginLoad);

user_route.get('/login',auth.isLogout,userController.loginLoad)

user_route.post('/login',userController.verifyLogin)
user_route.get('/home',auth.isLogin,userController.loadHome)

user_route.get('/logout',auth.isLogin,userController.userLogout)

//routes for forgot password
user_route.get('/forgot',auth.isLogout,userController.forgotLoad)
user_route.post('/forgot',userController.forgotVerify);

//showing forgot-password view
user_route.get('/forgot-password',auth.isLogout,userController.forgotPasswordLoad)
user_route.post('/forgot-password',userController.resetPassword)

//routes for getting signup confirmation page
user_route.get('/signupConfirmation',auth.isLogout, userController.loadSignupConfirmation);

//routes for getting forgot password confirmation page
user_route.get('/ForgotConfirmationPage',auth.isLogout, userController.loadForgotConfirmation);

//Routes for getting reset confirmation page
user_route.get('/ResetConfirmationPage',auth.isLogout, userController.loadResetConfirmation);

//Rotues for getting movie page
user_route.get('/movie',auth.isLogin, userController.loadMoviePage);

//routes to get profile page
user_route.get('/profile',auth.isLogin,userController.loadProfile)

//getting movie details
user_route.get('/movieDetails/:id',auth.isLogin,userController.loadMovieDetais)

//getting booking page
user_route.get('/booking/:id',auth.isLogin,userController.loadBookingPage)

//getting theater page
user_route.get('/theater',userController.loadTheaterPage)

//getting contact us page
user_route.get('/contactUs',userController.loadContactUspage)
user_route.post('/contactUs',userController.insertContactUs);

//exporting user_route 
module.exports =user_route;